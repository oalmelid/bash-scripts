# Shell script course

This jupyter notebook is a translation and slight modification of a course originally given by steinarh, the original notes can be found [here](http://www.pvv.ntnu.no/~steinarh/shkurs/kurs.html).

It's designed to run with the jupyter bash kernel, and the slides can be displayed using the jupyter RISE plugin.

## Initial setup

The script [create_env.sh] can be used to create a conda environment (named shell_scripts by default) with the required bash kernel and plugins. Modify it and run as needed.