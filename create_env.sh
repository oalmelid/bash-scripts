#!/bin/bash
CONDA_ENV="shell_scripts"

if [ -z "$(conda env list | grep $CONDA_ENV)" ]
then
	yes | conda create -n $CONDA_ENV python=3.6 ipython notebook
	source activate $CONDA_ENV
	yes | conda install -c conda-forge rise
	yes | pip install bash_kernel
	python -m bash_kernel.install
	# Strictly speaking this probably isn't needed, since
	# the parent shell won't inherit the environment variables.
	conda deactivate
	echo -e "Environment initialised. To use it, run"
	echo -e "source activate $CONDA_ENV"
else
	echo "Environment \"$CONDA_ENV\" already exists. Exiting"
fi

